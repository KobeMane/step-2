We are pleased to present you with the code editor of the future. We call it "Fork App".

Its main goal is to make the web development process faster, better and more convenient for you. Once you've tried it, you won't want to use any other tools.

v. 2.8 avaible For MacOS(Catalina and younger) and Windows(7 and younger);

technologies that we used: HTML, SCSS, JS, GULP, NODE.JS, CSS, Adaptive Layout;

project participants: Nikolya Kulchitskyi, Vladislove Shakhov;

Nikolya was responsible for that part of the project: Section "Revolutionary Editor", Section "Present" and Section "Fork Subscription Pricing";

Vladislove was responsible for that part of the project: "Header", Section "Fork App", Section "People Are Talking About Fork";



