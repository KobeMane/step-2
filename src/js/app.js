// POSTS SECTION

const posts = document.querySelectorAll(".post");
const postsArray = [...posts];

const postsImagesArray = [
  "@img/post-img 1.png",
  "@img/post1img 1.png",
  "@img/post4img 1.png",
  "@img/post6img 1.png",
];

postsArray.forEach((post, postIndex) => {
  const imageWrapper = document.createElement("div");
  const image = document.createElement("img");
  imageWrapper.classList.add("post__image-wrapper");
  image.classList.add("post__image");
  image.src = postsImagesArray[postIndex];
  imageWrapper.append(image);

  const postContent = document.createElement("div");
  const postHeadline = document.createElement("span");
  const postInfo = document.createElement("span");
  const postText = document.createElement("p");
  postContent.classList.add("post__content");
  postHeadline.classList.add("content-headline");
  postInfo.classList.add("content-info");
  postText.classList.add("content-text");

  postHeadline.innerText = "Aenean Adipiscing Etiam Vestibulum";
  postInfo.innerText = "Photography, Journal   /   9 Comments";
  postText.innerText =
    "Etiam porta sem malesuada euismod. Aenean leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.";

  postContent.append(postHeadline, postInfo, postText);
  post.append(imageWrapper, postContent);
});

// BURGER BUTTON

const menu = document.querySelector(".menu-wrapper");
const burger = document.querySelector(".menu-button-wrapper");
const burgerImg = document.querySelector(".menu-button");

let isMenuShow = false;
burger.addEventListener("click", function () {
  if (isMenuShow === false) {
    menu.style.display = "block";
    burgerImg.src = "@img/menu-button-close.png";
    console.log(menu.src);
    isMenuShow = true;
  } else if (isMenuShow === true) {
    menu.style.display = "none";
    burgerImg.src = "@img/menu-button-open.png";
    isMenuShow = false;
  }
});
